package info.konopelski.sportowybytom.pl.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

public class Utils {

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	/**
	 * Reading XML file from local storage
	 * 
	 * @return
	 */
	public static String loadXmlFile(String filename, Context context) {
		try {
			// FileInputStream fis = context.openFileInput("rssfeed.xml");
			FileInputStream fis = context.openFileInput(filename);
			BufferedReader r = new BufferedReader(new InputStreamReader(fis));
			String line = "";
			StringBuilder text = new StringBuilder();
			while ((line = r.readLine()) != null) {
				text.append(line);
			}
			r.close();
			return text.toString();
		} catch (FileNotFoundException e) {
			// Log.i(TAG, "loadXmlFile - FileNotFoundException");
			Toast.makeText(context, "Nie znaleziono pliku XML",
					Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			e.printStackTrace();
			// Log.i(TAG, "loadXmlFile - IOException");
		}
		return null;
	}

	/**
	 * Delete XML file, for development usage only
	 * 
	 * @return boolean
	 */
	public static boolean deleteXmlFile(String fileName, Context context) {
		boolean deleted = context.deleteFile(fileName);
		// Log.v(context.getTag(), "delete: " + deleted);
		Toast.makeText(context, "kasowanie XML", Toast.LENGTH_LONG).show();
		return deleted;
	}

	public static void streamWrite(String rssUrl, String rssFileName, Context context) {
		Log.d(Constans.SPORTOWY_BYTOM_UTILS, rssUrl + " / " + rssFileName);
		try {
			URL url = new URL(rssUrl);
			URLConnection connection = url.openConnection();
			connection.connect();
			InputStream input = new BufferedInputStream(url.openStream());
			OutputStream output = context.openFileOutput(rssFileName,
					Context.MODE_PRIVATE);
			byte data[] = new byte[1024];
			int count;
			while ((count = input.read(data)) != -1) {
				output.write(data, 0, count);
			}
			output.flush();
			output.close();
			input.close();

		} catch (Exception e) {
			// TODO: osluga
		}
	}
	
	/**
	 * Method to check network connection
	 * 
	 */
	public static boolean haveNetworkConnection(ConnectivityManager connectivityManager) {		
		NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
		if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
		    //Toast.makeText(context, "No Internet connection!", Toast.LENGTH_LONG).show();
		    return false;
		}
		return true;
	}
}