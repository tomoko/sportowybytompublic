package info.konopelski.sportowybytom.pl.utils;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class RssparserUtils {

	
	/**
	 * Return map of RSS items
	 * 
	 * @param rssstring
	 * @return
	 */
	public static List<HashMap<String, String>> parse(String rssstring) {
		
		List<HashMap<String, String>> itemsMap = new ArrayList<HashMap<String, String>>();		
		try {
			if (rssstring.length() > 100) {
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				Document doc;
				InputSource is = new InputSource(new StringReader(rssstring));
				doc = db.parse(is);
				doc.getDocumentElement().normalize();
				NodeList itemLst = doc.getElementsByTagName("item");

				for (int i = 0; i < itemLst.getLength(); i++) {
					HashMap<String, String> map = new HashMap<String, String>();

					Node item = itemLst.item(i);
					if (item.getNodeType() == Node.ELEMENT_NODE) {
						Element ielem = (Element) item;
						NodeList title = ielem.getElementsByTagName("title");
						NodeList link = ielem.getElementsByTagName("link");
						NodeList pubDate = ielem.getElementsByTagName("pubDate");
						NodeList category = ielem.getElementsByTagName("category");
						NodeList description = ielem.getElementsByTagName("description");
						NodeList image = ielem.getElementsByTagName("image");
			
						map.put( "title", title.item(0).getChildNodes().item(0).getNodeValue() );
						map.put( "link", link.item(0).getChildNodes().item(0).getNodeValue() );
						map.put( "pubDate", pubDate.item(0).getChildNodes().item(0).getNodeValue() );
						map.put( "category", category.item(0).getChildNodes().item(0).getNodeValue() );
						map.put( "description", description.item(0).getChildNodes().item(0).getNodeValue() );
						//map.put( "image", image.item(0).getChildNodes().item(0).getNodeValue().toString() );
						
						if ( image.item(0).getChildNodes().item(0).getNodeValue().length() > 3) {
							map.put( "image", image.item(0).getChildNodes().item(0).getNodeValue());
						} else {
							map.put( "image", "" );
						}
						
						String dateString =  pubDate.item(0).getChildNodes().item(0).getNodeValue(); 
						/*		TODO parse date
						//DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
						//DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss ZZZZZ");
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy MM dd", Locale.US);
						Date pubDateText = null;
						
						try {
							pubDateText = formatter.parse( dateString.trim() );
						} catch ( java.text.ParseException e ) {
							//e.printStackTrace();
							System.out.println(e.getMessage() );
				            //secondTV.setText("Error: " + e.toString());
				        }
						if (pubDateText == null) {
							//dateString=dateString;
						} else {
							dateString = pubDateText.toString();
						}
						//String pubDateText = formatter.parse( pubDate.item(0).getChildNodes().item(0).getNodeValue().toString() );
						 */		
						 
						String dscr = description.item(0).getChildNodes().item(0).getNodeValue().toString();
						
						dscr.replace("\u00a0","");
						
						map.put( "secondline",  android.text.Html.fromHtml(dscr).toString().trim()); // html clean - This has made my day 
						
						map.put( "thirdline", category.item(0).getChildNodes().item(0).getNodeValue() + " - " + dateString);

						map.put( "link", link.item(0).getChildNodes().item(0).getNodeValue() );
						
						itemsMap.add(map);
					}
				}
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return itemsMap;
	}
}
