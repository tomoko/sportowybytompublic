package info.konopelski.sportowybytom.pl.utils;

import android.content.Context;

/**
 *  Singleton, ktory trzyma context aplikacji - obecnie nie
 *  uzywany, aplikacja jest zbyt mala i wszedzie do contekstu jest dostep
 *  
 */
public class MyContext {

	private Context context;
	private static MyContext myContext;

	private MyContext() {
	}

	public static MyContext getInstandce() {
		if (myContext != null) {
			myContext = new MyContext();
		}
		return myContext;
	}

	public Context getContext() {		
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

}
