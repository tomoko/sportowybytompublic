package info.konopelski.sportowybytom.pl.utils;

public interface Constans {
	
	//TAB names
	public static final String SPORTOWY_BYTOM_MAIN_TAB = "SportowybytomMain";
	public static final String TAB_ACTIBITY_MAIN_TAB = "TabActivityMain";
	public static final String SPORTOWY_BYTOM_IMAGE_lOADER_TAB = "Sportowybytom-ImageLoader";
	public static final String SPORTOWY_BYTOM_LAZY_ADAPTER_TAB = "Sportowybytom-LazyAdapter";
	public static final String SPORTOWY_BYTOM_RSS_DOWNLOADER_TAB = "Sportowybytom-RSSDownloader";
	public static final String SPORTOWY_BYTOM_YOU_TUBE_TAB = "Sportowybytom-YouTubeMain";
	public static final String SPORTOWY_BYTOM_YOU_TUBE_DOWNLOADER_TAB = "Sportowybytom-YoutubeDownloader";
	public static final String SPORTOWY_BYTOM_YOU_TUBE_ADAPTER_TAB = "Sportowybytom-YoutubeAdapter";
	public static final String SPORTOWY_BYTOM_UTILS = "Sportowybytom-Utils";
	public static final String SPORTOWY_CACHE_DIR = "sportowybytom_cache";
	
	//URL's
	public static final String RSS_URL_SPORTOEY_BYTOM = "http://sportowybytom.pl/rss/rss.xml";
	public static final String RSS_URL_YOUTUBE = "http://gdata.youtube.com/feeds/mobile/users/mimasstudio/uploads?v=2&alt=jsonc";
	public static final String URL_FACEBOOK = "http://www.facebook.com/pages/sportowybytompl/342908505784386";
	public static final String URL_SPOTOWYBYTOM = "http://sportowybytom.pl";
	
	//File XML
	public static final String RSS_FILE_NAME = "rssfeed.xml";
	public static final String YOUTUBE_XML = "youtube.xml";
	
	
	
	
	

}
