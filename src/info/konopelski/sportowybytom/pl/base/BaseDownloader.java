package info.konopelski.sportowybytom.pl.base;

import info.konopelski.sportowybytom.pl.utils.Utils;
import android.app.Activity;
import android.os.AsyncTask;

public abstract class BaseDownloader extends AsyncTask<String, Void, String> {

	private Activity activity;
	private String fileName;

	public BaseDownloader(Activity activity, String fileName) {
		super();
		this.activity = activity;
		this.fileName = fileName;
	}

	@Override
	protected String doInBackground(String... params) {
		Utils.streamWrite((String) params[0], this.fileName,
				this.activity.getApplicationContext());
		// TODO czemu return null;
		return null;

	}
	
}
