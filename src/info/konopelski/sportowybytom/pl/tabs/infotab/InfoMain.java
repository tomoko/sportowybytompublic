package info.konopelski.sportowybytom.pl.tabs.infotab;

import info.konopelski.sportowybytom.pl.R;
import info.konopelski.sportowybytom.pl.utils.Constans;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

/**
 * Klasa tworzy zakadke INFO
 * 
 */
public class InfoMain extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.infotab);

		ImageButton facebookImageButton = (ImageButton) findViewById(R.id.facebookImageButton);
		facebookImageButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent browserIntent = new Intent(
						Intent.ACTION_VIEW,
						Uri.parse(Constans.URL_FACEBOOK));
				startActivity(browserIntent);
			}
		});
		Button wwwButton = (Button) findViewById(R.id.wwwButton);
		wwwButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse(Constans.URL_SPOTOWYBYTOM));
				startActivity(browserIntent);
			}
		});

	}

}
