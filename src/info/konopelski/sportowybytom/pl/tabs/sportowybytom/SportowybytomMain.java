package info.konopelski.sportowybytom.pl.tabs.sportowybytom;

import info.konopelski.sportowybytom.pl.R;
import info.konopelski.sportowybytom.pl.tabs.alerts.InfoAlertMain;
import info.konopelski.sportowybytom.pl.tabs.alerts.InfoNoNetwork;
import info.konopelski.sportowybytom.pl.tabs.youtube.YouTubeMain;
import info.konopelski.sportowybytom.pl.utils.Constans;
import info.konopelski.sportowybytom.pl.utils.RssparserUtils;
import info.konopelski.sportowybytom.pl.utils.Utils;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Rss Sportowybytom
 * 
 * http://www.androidhive.info/2012/05/android-rss-reader-application-using-
 * sqlite-part-2/
 * http://stackoverflow.com/questions/5320358/update-listview-dynamically
 * -with-adapter?lq=1 http://erikw.eu/android-update-single-item-in-listview/
 * http
 * ://thinkandroid.wordpress.com/2012/06/13/lazy-loading-images-from-urls-to-
 * listviews/ https://github.com/thest1/LazyList
 * 
 * http://stackoverflow.com/questions/11443072/adapter-nevers-enter-the-getview-
 * method
 * 
 * 
 * TODO - sprawdzanie czy jest siec - youtube activity
 * 
 */

/**
 * Tworzy zakladke SportowyBytom
 * 
 * 
 */
public class SportowybytomMain extends Activity {

	private ListView listViewMain;
	private SportowybytomAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Log.v(Constans.SPORTOWY_BYTOM_MAIN_TAB, "onCreate");
		listViewMain = (ListView) findViewById(R.id.listViewMain);
		listViewMain.setClickable(true);

		// deleteXmlFile(); // DELETE xml

		if (!Utils
				.haveNetworkConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			return;
		}

		File file = getApplicationContext().getFileStreamPath("rssfeed.xml");
		if (file.exists()) {
			updateListview();
		} else {
			SportowybytomRSSDownloader task = new SportowybytomRSSDownloader(this);
			task.execute(new String[] { Constans.RSS_URL_SPORTOEY_BYTOM });
		}
	}

	/*
	 * na needed right now
	 */
	protected void onResume() {
		super.onResume();
		Log.v(Constans.SPORTOWY_BYTOM_MAIN_TAB, "onResume");
		if (!Utils
				.haveNetworkConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.internetDisabledTitle),
					Toast.LENGTH_LONG).show();
			return;
		}
	}

	/**
	 * NAJNOWSZA
	 */
	public void updateListview() {
		Log.d(Constans.SPORTOWY_BYTOM_MAIN_TAB, "RSS_FILE_NAME: " + Constans.RSS_FILE_NAME);
		String xml = Utils.loadXmlFile(Constans.RSS_FILE_NAME, getApplicationContext());
		Log.d(Constans.SPORTOWY_BYTOM_MAIN_TAB, xml);
		
		// loadXmlFile( RSSDownloader.rssFileName );
		List<HashMap<String, String>> rssMapStatic = RssparserUtils.parse(xml);
		// Log.v(TAG, rssMapStatic.toString());
		adapter = new SportowybytomAdapter(this, rssMapStatic);
		listViewMain.setAdapter(adapter);
		listViewMain
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						String textViewId = ((TextView) view
								.findViewById(R.id.link)).getText().toString();
						Log.d(Constans.SPORTOWY_BYTOM_MAIN_TAB,
								"textViewId - onItemClick: " + textViewId);						
						startActivity(new Intent(Intent.ACTION_VIEW, Uri
								.parse(textViewId)));
					}
				});
	}

	/**
	 * Hardrive menu handler
	 * 
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem element) {

		switch (element.getItemId()) {

		case R.id.updatedfeed:

			SportowybytomRSSDownloader task = new SportowybytomRSSDownloader(this);
			task.execute(new String[] { Constans.RSS_URL_SPORTOEY_BYTOM });

			break;
		case R.id.parsefeed:

			// updateListview3(); // refesh listview
			showYoutubeIntent();

			break;
		case R.id.testmenu:
			// deleteXmlFile(); // for testing
			Intent info = new Intent(this, InfoAlertMain.class);
			InfoNoNetwork.showInfoApp(info, getApplicationContext());
			startActivity(info);

			break;

		}
		return true;

	}

	private void showYoutubeIntent() {

		Intent youtubeIntent = new Intent(this, YouTubeMain.class);
		startActivity(youtubeIntent);

	}

}