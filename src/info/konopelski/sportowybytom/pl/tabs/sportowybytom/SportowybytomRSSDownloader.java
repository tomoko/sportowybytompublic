package info.konopelski.sportowybytom.pl.tabs.sportowybytom;

import info.konopelski.sportowybytom.pl.base.BaseDownloader;
import info.konopelski.sportowybytom.pl.utils.Constans;
import android.R;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

public class SportowybytomRSSDownloader extends BaseDownloader {

	private SportowybytomMain sportowybytomMain;

	public SportowybytomRSSDownloader(SportowybytomMain sportowybytomMain) {
		super(sportowybytomMain, Constans.RSS_FILE_NAME);
		this.sportowybytomMain = sportowybytomMain;		
		
	}
	

	@Override
	protected void onPostExecute(String result) {
		//Toast.makeText(this.sportowybytomMain.getApplicationContext(), this.sportowybytomMain.getApplicationContext().getString(R.string.untitled ), Toast.LENGTH_LONG).show();
		Toast.makeText(this.sportowybytomMain.getApplicationContext(), "Zaaktualizowano wiadomości", Toast.LENGTH_LONG).show();
		Log.d(Constans.SPORTOWY_BYTOM_RSS_DOWNLOADER_TAB, "=========== onPostExecute");
		this.sportowybytomMain.updateListview();
	}
}