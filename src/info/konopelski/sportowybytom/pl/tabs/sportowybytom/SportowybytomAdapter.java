package info.konopelski.sportowybytom.pl.tabs.sportowybytom;

import info.konopelski.sportowybytom.pl.R;
import info.konopelski.sportowybytom.pl.utils.Constans;
import info.konopelski.sportowybytom.pl.utils.ImageLoader;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Tworzy zawartosc zakladki SportowyBytom
 * 
 */
public class SportowybytomAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater inflater;
	private ImageLoader imageLoader;
	private List<HashMap<String, String>> mData;

	public SportowybytomAdapter(Activity a, List<HashMap<String, String>> data) {
		activity = a;
		mData = data;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(activity.getApplicationContext());
	}

	@Override
	public int getCount() {
		Log.d(Constans.SPORTOWY_BYTOM_LAZY_ADAPTER_TAB,
				"getCount " + mData.size());
		return mData.size();
	}

	@Override
	public Object getItem(int position) {

		Log.d(Constans.SPORTOWY_BYTOM_LAZY_ADAPTER_TAB, "getItem Object "
				+ position);
		Log.d(Constans.SPORTOWY_BYTOM_LAZY_ADAPTER_TAB, "getItem mData.get "
				+ mData.get(position));
		mData.get(position);
		return position;
	}

	@Override
	public long getItemId(int position) {

		Log.d(Constans.SPORTOWY_BYTOM_LAZY_ADAPTER_TAB, "getItemId long "
				+ position);
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.list_item, null);

		TextView text = (TextView) vi.findViewById(R.id.item_title); // title
		TextView text2 = (TextView) vi.findViewById(R.id.item2_title); // dscr
		TextView item3_title = (TextView) vi.findViewById(R.id.item3_title); // category
		ImageView image = (ImageView) vi.findViewById(R.id.item_img);
		TextView link = (TextView) vi.findViewById(R.id.link);

		text.setText(mData.get(position).get("title"));
		text2.setText(mData.get(position).get("secondline"));
		link.setText(mData.get(position).get("link"));
		// item3_title.setText( mData.get(position).get("thirdline") );
		
		// parsowanie daty
        final DateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH);
        DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;
        try {
                date = format.parse( mData.get(position).get("pubDate") );
        } catch (ParseException e) {
                e.printStackTrace();
        }
        final String outputDate = format2.format(date);

	
        item3_title.setText( Html.fromHtml( "<font color='#F79426'><b>" + mData.get(position).get("category")  + "</b></font> " + outputDate ) );

        /*		
		item3_title
				.setText(mData.get(position).get("category")
						+ " - "
						+ mData.get(position)
								.get("pubDate")
								.substring(
										0,
										mData.get(position).get("pubDate")
												.length() - 14));
 		*/

		if (mData.get(position).get("image").length() > 10) {
			imageLoader.DisplayImage(mData.get(position).get("image"), image);
		}

		return vi;
	}

}
