package info.konopelski.sportowybytom.pl.tabs.youtube;

import info.konopelski.sportowybytom.pl.base.BaseDownloader;
import info.konopelski.sportowybytom.pl.utils.Constans;
import android.util.Log;
import android.widget.Toast;

public class YoutubeDownloader extends BaseDownloader {

	private YouTubeMain youTubeMain;

	public YoutubeDownloader(YouTubeMain youTubeMain) {
		super(youTubeMain, Constans.YOUTUBE_XML);
		this.youTubeMain = youTubeMain;
	}

	@Override
	protected void onPostExecute(String result) {
		Toast.makeText(this.youTubeMain.getApplicationContext(), "Zaaktualizowano kanał YouTube", Toast.LENGTH_LONG).show();
		//TODO przeniesc napis do strings
		Log.d(Constans.SPORTOWY_BYTOM_YOU_TUBE_DOWNLOADER_TAB, "=========== onPostExecute YoutubeDownloader");
		this.youTubeMain.updateYoutube();
	}
}