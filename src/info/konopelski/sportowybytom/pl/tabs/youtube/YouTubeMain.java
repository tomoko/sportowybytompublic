package info.konopelski.sportowybytom.pl.tabs.youtube;

import info.konopelski.sportowybytom.pl.R;
import info.konopelski.sportowybytom.pl.utils.Constans;
import info.konopelski.sportowybytom.pl.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class YouTubeMain extends Activity {

	public ListView listViewYoutube;
	public static YoutubeAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.youtube_main);

		listViewYoutube = (ListView) findViewById(R.id.listViewYoutube);
		listViewYoutube.setClickable(true);

		File file = getApplicationContext().getFileStreamPath(
				Constans.YOUTUBE_XML);

		if (file.exists()) {
			updateYoutube();
		} else {
			YoutubeDownloader task = new YoutubeDownloader(this);
			task.execute(new String[] { Constans.RSS_URL_YOUTUBE });
		}

	}

	protected void onResume() {
		super.onResume();
		
	}
	
	public void updateYoutube() {

		String jsonString = Utils.loadXmlFile(Constans.YOUTUBE_XML, getApplicationContext());

		List<HashMap<String, String>> itemsMap = new ArrayList<HashMap<String, String>>();

		try {
			JSONObject json = new JSONObject(jsonString);
			JSONObject dataObject = json.getJSONObject("data");
			JSONArray jsonArray = dataObject.getJSONArray("items");

			Log.d(Constans.SPORTOWY_BYTOM_YOU_TUBE_TAB,
					"COUNT: " + jsonArray.length());

			for (int i = 0; i < jsonArray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();

				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String title = jsonObject.getString("title");
				String description = jsonObject.getString("description");
				String thumbUrl = jsonObject.getJSONObject("thumbnail")
						.getString("sqDefault");
				String videoUrl = jsonObject.getJSONObject("player").getString(
						"default");

				/*
				 * System.out.println(title); System.out.println(thumbUrl);
				 * System.out.println(videoUrl);
				 * 
				 * final JSONObject item = jsonArray.getJSONObject(i); final
				 * JSONObject player = item.getJSONObject("player"); final
				 * String url = player.getString("default"); Log.d(TAG, "url: "
				 * + url);
				 */

				map.put("title", title);
				map.put("secondline", android.text.Html.fromHtml(description)
						.toString().trim());
				map.put("link", videoUrl);
				map.put("image", thumbUrl);
				map.put("category",
						"wyświetleń: " + jsonObject.getString("viewCount"));

				itemsMap.add(map);

			}

			// Log.d(TAG, itemsMap.toString());
			adapter = new YoutubeAdapter(this, itemsMap);
			listViewYoutube.setAdapter(adapter);

			listViewYoutube
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							String textViewId = ((TextView) view
									.findViewById(R.id.link)).getText()
									.toString();
							Log.d(Constans.SPORTOWY_BYTOM_YOU_TUBE_TAB,
									"textViewId - onItemClick: " + textViewId);
							startActivity(new Intent(
									Intent.ACTION_VIEW, Uri.parse(textViewId)));
						}
					});

		} catch (JSONException e) {
			Log.d(Constans.SPORTOWY_BYTOM_YOU_TUBE_TAB,
					"JSONException: " + e.getMessage());
			e.printStackTrace();

		}

	}

}
