package info.konopelski.sportowybytom.pl.tabs.youtube;

import info.konopelski.sportowybytom.pl.R;
import info.konopelski.sportowybytom.pl.utils.Constans;
import info.konopelski.sportowybytom.pl.utils.ImageLoader;

import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class YoutubeAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater inflater;
	public ImageLoader imageLoader;
	private List<HashMap<String, String>> mData;	

	public YoutubeAdapter(Activity a, List<HashMap<String, String>> data) {
		activity = a;
		mData = data;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(activity.getApplicationContext());
		
		//Log.d(TAG, "=========== YoutubeAdapter  ");
	}


	@Override
	public int getCount() {
		//Log.d(TAG, "getCount " + mData.size());
		return mData.size();
	}
	
	@Override
	public Object getItem(int position) {
		
		Log.d(Constans.SPORTOWY_BYTOM_YOU_TUBE_ADAPTER_TAB, "getItem Object " + position);
		Log.d(Constans.SPORTOWY_BYTOM_YOU_TUBE_ADAPTER_TAB, "getItem mData.get " + mData.get(position));
		mData.get(position);
		return position;
	}



	@Override
	public long getItemId(int position) {

		//Log.d(TAG, "getItemId long " + position);
		return position;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.youtube_item, null);
		
		try {
			TextView text = (TextView) vi.findViewById(R.id.item_title); // title
			TextView text2 = (TextView) vi.findViewById(R.id.item2_title); // dscr
			TextView item3_title = (TextView) vi.findViewById(R.id.item3_title); // category
			ImageView image = (ImageView) vi.findViewById(R.id.item_img);
			TextView link = (TextView) vi.findViewById(R.id.link);
			
			text.setText(mData.get(position).get("title"));
			text2.setText(mData.get(position).get("secondline"));
			link.setText(mData.get(position).get("link"));
			item3_title.setText(mData.get(position).get("category"));

			if (mData.get(position).get("image").length() > 10) {
				imageLoader.DisplayImage(mData.get(position).get("image"), image);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return vi;
	}

}
