package info.konopelski.sportowybytom.pl.tabs.alerts;

import info.konopelski.sportowybytom.pl.R;
import android.content.Context;
import android.content.Intent;

/**
 * Klasa wprowadza tekst do alertow i informacji
 * 
 * @author tomek
 */
public class InfoNoNetwork {
	
	private InfoNoNetwork() {
	}

	/**
	 * Tworzy alert o braku polaczenia

	 * @param info
	 * @param context
	 */
	public static void showNoNetworkConnection(Intent info, Context context ) {
		String infoStr = "";
		infoStr += context.getString(R.string.internetDisabled);
		infoStr += "\n\n";
		infoStr += "\n\n";
		infoStr += context.getString(R.string.shortAppInfo);
		infoStr += "\n\n";
		info.putExtra("text", infoStr);
		info.putExtra("title", context.getString(R.string.internetDisabledTitle));		
	}
	
	/**
	 * Tworzy info o aplikacji i autorze

	 * @param info
	 * @param context
	 */
	public static void showInfoApp(Intent info, Context context) {
		String infoStr = "";
		infoStr += context.getString(R.string.shortAppInfo);
		infoStr += context.getString(R.string.shortAppInfoMore); // link, author
		info.putExtra("text", infoStr);
		info.putExtra("title", "Info");		
	}
	
}
