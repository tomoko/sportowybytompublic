package info.konopelski.sportowybytom.pl.tabs.alerts;

import info.konopelski.sportowybytom.pl.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Klasa obsuguje alerty i informacje
 *
 */
public class InfoAlertMain extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.info);

		Bundle extras;
		extras = getIntent().getExtras();

		String text = extras.getString("text");
		String title = extras.getString("title");

		if (text.length() > 2) {
			TextView infoText = (TextView) findViewById(R.id.infoText);
			infoText.setText(text);
		}
		if (title.length() > 0) {
			setTitle(title);
		}
	}
	
}