package info.konopelski.sportowybytom.pl;

import info.konopelski.sportowybytom.pl.tabs.alerts.InfoAlertMain;
import info.konopelski.sportowybytom.pl.tabs.alerts.InfoNoNetwork;
import info.konopelski.sportowybytom.pl.tabs.infotab.InfoMain;
import info.konopelski.sportowybytom.pl.tabs.sportowybytom.SportowybytomMain;
import info.konopelski.sportowybytom.pl.tabs.youtube.YouTubeMain;
import info.konopelski.sportowybytom.pl.utils.Constans;
import info.konopelski.sportowybytom.pl.utils.Utils;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

/**
 * Bazowa klasa aplikacji inicjule zakladki
 * 
 * @author tomek
 */
public class TabActivityMain extends TabActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabhost_main);
		
		Log.d(Constans.TAB_ACTIBITY_MAIN_TAB, "isInternetEnabled: " + Utils.haveNetworkConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)));
		if (!Utils.haveNetworkConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			showNoNetworkConnection();
		}

		TabHost tabHost = getTabHost();

		// Tab Aktualnosci
		TabSpec sitespec = tabHost.newTabSpec(String.valueOf(R.string.news));
		sitespec.setIndicator(getString(R.string.news), getResources().getDrawable(R.drawable.icon_site_tab));
		Intent siteIntent = new Intent(this, SportowybytomMain.class);
		sitespec.setContent(siteIntent);

		// Tab Filmy Youtube
		TabSpec youtubespec = tabHost.newTabSpec(String.valueOf(R.string.youtube));
		youtubespec.setIndicator(getString(R.string.youtube),
				getResources().getDrawable(R.drawable.icon_youtube_tab));
		Intent youtubeIntent = new Intent(this, YouTubeMain.class);
		youtubespec.setContent(youtubeIntent);

		// Tab Informacje
		TabSpec infospec = tabHost.newTabSpec(String.valueOf(R.string.info));
		infospec.setIndicator(getString(R.string.info), getResources().getDrawable(R.drawable.icon_info_tab));
		Intent infoIntent = new Intent(this, InfoMain.class);
		infospec.setContent(infoIntent);

		tabHost.addTab(sitespec);
		tabHost.addTab(youtubespec);
		tabHost.addTab(infospec);
	}

	/**
	 * NoNet window
	 */
	private void showNoNetworkConnection() {

		Intent info = new Intent(this, InfoAlertMain.class);
		InfoNoNetwork.showNoNetworkConnection(info, getApplicationContext());
		startActivity(info);
	}

}
